﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.Log;

namespace WeavingProdControl.DataAccess
{
    public class PeaksPerCmDao : TimeRelevant
    {
        private static PeaksPerCmDao instance;

        public static PeaksPerCmDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new PeaksPerCmDao();
                return instance;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.PeaksPerCmRefreshTime;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                lock (this.peaksPerCmLock)
                {
                    return (this.peaksPerCm == null);
                }
            }
        }

        private List<Tejido> peaksPerCm;
        private object peaksPerCmLock;
        private PeaksPerCmDao()
        {
            this.peaksPerCmLock = new object();
            this.peaksPerCm = null;
        }

        public List<Tejido> AllData()
        {
            if (ShouldRefresh)
            {
                using (var Context = new CPTelaresEntities())
                {
                    var requestQuery = from peaks in Context.Tejidos
                                       select peaks;
                    try
                    {
                        lock (this.peaksPerCmLock)
                        {
                            this.peaksPerCm = requestQuery.ToList<Tejido>();
                        }
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            lock (this.peaksPerCmLock)
            {
                if (this.peaksPerCm != null)
                    return new List<Tejido>(this.peaksPerCm);
                else
                    EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " List<Tejido> peaksPerCm is null");
                return new List<Tejido>();
            }
        }

        public double PeaksPerItem(string itemNumber)
        {
            if (itemNumber != null)
            {
                Tejido foundData = AllData().Find(peaks => peaks.matricula.Trim().ToUpper() == itemNumber.Trim().ToUpper());
                if (foundData != null)
                {
                    if (foundData.PASCMTelar != null)
                        return (double)foundData.PASCMTelar;
                    else
                        EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " Tejido foundData.PASCMTelar is null");
                }
                else
                    EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " Tejido foundData is null");
            }
            else
                EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " string itemNumber is null");
            return 0;
        }
    }
}
