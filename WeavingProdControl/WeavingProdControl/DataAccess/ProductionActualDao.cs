﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.DataManage;
using WeavingProdControl.Log;

namespace WeavingProdControl.DataAccess
{
    public class ProductionActualDao
    {
        private static ProductionActualDao instance;

        public static ProductionActualDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new ProductionActualDao();
                return instance;
            }
        }

        private ProductionActualDao()
        {

        }

        private void RunningToRunning(ProduccionActual currentData, AdamMachineData data)
        {
            if (currentData.Pasadas > data.Pasadas)
                EventLogMultiThread.AddWarningLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " sql data peaks are bigger than Adam data Database:" + currentData.Pasadas + " Adam:" + data.Pasadas);
            currentData.Pasadas = data.Pasadas;
            currentData.UltimaActualizacion = data.ReadTime;
            currentData.UltimaActualizacion = data.ReadTime;
            if (PeaksPerCmDao.Instance.PeaksPerItem(currentData.Articulo) != 0)
                currentData.MetrosFabricados = Math.Round(data.Pasadas / PeaksPerCmDao.Instance.PeaksPerItem(currentData.Articulo) / 100, Properties.Settings.Default.ProducedMetersDecimalCount);
            else
                currentData.MetrosFabricados = 0;
            currentData.PasadasCMTelar = (int)PeaksPerCmDao.Instance.PeaksPerItem(currentData.Articulo);
            currentData.Velocidad = SpeedCounter.Calculate(currentData, data);
        }

        private void RunningToStopped(ProduccionActual currentData, AdamMachineData data, CPTelaresEntities Context)
        {
            ProductionActualToProduction adapter = new ProductionActualToProduction(currentData, data);
            Produccion newProdHistData = adapter.Adapted();
            if (newProdHistData != null)
                Context.Produccions.Add(newProdHistData);
            else
                EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " Produccion newProdHistData is null ");
            currentData.Inicio = data.ReadTime;
            currentData.UltimaActualizacion = data.ReadTime;
            currentData.Estado = MachineStatusControl.StopStatus();
            currentData.Velocidad = 0;
            currentData.MetrosFabricados = 0;
            currentData.idCodigoParo = data.StopReason;
            currentData.CodigoParo = data.StopReason;
        }

        private void StoppedToRunning(ProduccionActual currentData, AdamMachineData data, CPTelaresEntities Context)
        {
            ProductionActualToProduction adapter = new ProductionActualToProduction(currentData, data);
            Produccion newProdHistData = adapter.Adapted();
            if (newProdHistData != null)
                Context.Produccions.Add(newProdHistData);
            else
                EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " Produccion newProdHistData is null ");
            currentData.Inicio = data.ReadTime;
            currentData.UltimaActualizacion = data.ReadTime;
            currentData.Pasadas = data.Pasadas;
            if (PeaksPerCmDao.Instance.PeaksPerItem(currentData.Articulo) != 0)
                currentData.MetrosFabricados = Math.Round(data.Pasadas / PeaksPerCmDao.Instance.PeaksPerItem(currentData.Articulo) / 100, Properties.Settings.Default.ProducedMetersDecimalCount);
            else
                currentData.MetrosFabricados = 0;
            currentData.PasadasCMTelar = (int)PeaksPerCmDao.Instance.PeaksPerItem(currentData.Articulo);
            currentData.Estado = MachineStatusControl.RunningStatus();
            currentData.Velocidad = SpeedCounter.Calculate(currentData, data);
            currentData.idCodigoParo = data.StopReason;
            currentData.CodigoParo = data.StopReason;
        }

        private void StoppedToStopped(ProduccionActual currentData, AdamMachineData data)
        {
            currentData.UltimaActualizacion = data.ReadTime;
        }

        public bool UpdateProdData(List<AdamMachineData> machineData)
        {
            if (machineData != null)
            {
                using (var Context = new CPTelaresEntities())
                {
                    foreach (AdamMachineData data in machineData)
                    {
                        var requestQuery = from machine in Context.ProduccionActuals
                                           where machine.IdTelar == data.MachineId
                                           select machine;
                        try
                        {
                            ProduccionActual currentData = requestQuery.FirstOrDefault<ProduccionActual>();
                            if (currentData != null)
                            {
                                if (MachineStatusControl.IsRunning(currentData))
                                {
                                    if (data.Running)
                                        RunningToRunning(currentData, data);
                                    else
                                        RunningToStopped(currentData, data, Context);
                                }
                                else
                                {
                                    if (data.Running)
                                        StoppedToRunning(currentData, data, Context);
                                    else
                                        StoppedToStopped(currentData, data);
                                }
                            }
                            else
                                EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " ProduccionActual currentData is null " + data.MachineId);
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                    try
                    {
                        Context.SaveChanges();
#if DEBUG
                        Console.WriteLine("UpdateProdData(List<AdamMachineData> machineData) " + DateTime.Now);
#endif
                        return true;
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            string validationErorrs = "";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                validationErorrs += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage;
                            }
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(),
                                "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors: " + validationErorrs);
                        }
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            else
                EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " List<AdamMachineData> machineData is null");
            return false;
        }
    }
}
