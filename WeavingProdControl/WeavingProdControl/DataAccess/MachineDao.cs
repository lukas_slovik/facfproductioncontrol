﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.Log;

namespace WeavingProdControl.DataAccess
{
    public class MachineDao
    {
        private static MachineDao instance;

        public static MachineDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new MachineDao();
                return instance;
            }
        }

        private MachineDao()
        {

        }

        public List<Telare> AllMachines()
        {
            using (var Context = new CPTelaresEntities())
            {
                var requestQuery = from machine in Context.Telares
                                   where machine.Activo == true && machine.DireccionIP != null
                                   select machine;
                try
                {
                    return requestQuery.ToList<Telare>();
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
            return new List<Telare>();
        }

        public List<string> IpMachines()
        {
            List<Telare> machines = AllMachines();
            List<string> ipS = new List<string>();
            foreach (Telare machine in machines)
            {
                if (machine.DireccionIP != null)
                    ipS.Add(machine.DireccionIP.Trim());
            }
            return ipS;
        }
    }
}
