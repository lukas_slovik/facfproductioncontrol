﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.ServiceOperation;

namespace WeavingProdControl
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG
            ServiceOperations operations = new ServiceOperations();
            operations.StartServiceOperations();
#else
ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new WeavingService()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
