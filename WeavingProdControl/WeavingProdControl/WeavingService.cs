﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.ServiceOperation;

namespace WeavingProdControl
{
    public partial class WeavingService : ServiceBase
    {
        private ServiceOperations operations;
        public WeavingService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.operations = new ServiceOperations();
            this.operations.StartServiceOperations();
        }

        protected override void OnStop()
        {
            if (this.operations != null)
                this.operations.StopServiceOperations();
        }
    }
}
