﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WeavingProdControl.DataAccess;
using WeavingProdControl.DataManage;
using WeavingProdControl.ServiceOperation;

namespace WeavingProdControl.ProductionControl
{
    public class DatabaseSaver : OperationCore
    {
        public DatabaseSaver() : base(false, Properties.Settings.Default.DatabaseSaverCheckTime)
        {
        }

        public override void Start()
        {
            Thread operations = new Thread(RepeatedOperation);
            operations.Start();
        }

        public override void Stop()
        {
            ShouldStop = true;
        }

        protected override void RunOperation()
        {
            ProductionActualDao.Instance.UpdateProdData(AdamDataSaver.DataToSave.GetAll());
        }
    }
}
