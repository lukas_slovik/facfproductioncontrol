﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.DataAccess;
using WeavingProdControl.DataManage;
using WeavingProdControl.ServiceOperation;

namespace WeavingProdControl.ProductionControl
{
    public class IoModuleDataOperation : OperationCore
    {
        private List<MachineAdam> adamIoDevices;
        public IoModuleDataOperation() : base(true, null)
        {
            this.adamIoDevices = new List<MachineAdam>();
        }
        public override void Start()
        {
            List<Telare> machines = MachineDao.Instance.AllMachines();
            foreach (Telare machine in machines)
            {
                this.adamIoDevices.Add(new MachineAdam(machine));
            }
            foreach (MachineAdam idDevice in adamIoDevices)
                idDevice.IoConnector.Start();
        }

        public override void Stop()
        {
            foreach (MachineAdam idDevice in adamIoDevices)
                idDevice.IoConnector.Stop();
        }

        protected override void RunOperation()
        {
            throw new NotImplementedException();
        }
    }
}
