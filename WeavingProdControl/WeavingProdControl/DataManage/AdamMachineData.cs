﻿using AdamUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeavingProdControl.DataManage
{
    public class AdamMachineData
    {
        public int MachineId { get; set; }
        public int Pasadas { get; set; }
        public bool Running { get; set; }
        public int StopReason { get; set; }
        public DateTime ReadTime { get; set; }
        public AdamMachineData()
        {
            MachineId = -1;
            Pasadas = 0;
            Running = false;
            StopReason = -1;
        }
    }
}
