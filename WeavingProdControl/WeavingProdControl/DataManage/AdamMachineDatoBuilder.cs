﻿using AdamUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeavingProdControl.DataManage
{
    public class AdamMachineDatoBuilder
    {
        private static int LowerBit(string register)
        {
            string[] bitNumbers = register.Split(Properties.Settings.Default.AdamRegisterSplitter);
            if (bitNumbers.Length >= 2)
            {
                int lowerBitNumber = 0;
                if (Int32.TryParse(bitNumbers[1], out lowerBitNumber))
                    return lowerBitNumber;
            }
            return 0;
        }

        private static int UpperBit(string register)
        {
            string[] bitNumbers = register.Split(Properties.Settings.Default.AdamRegisterSplitter);
            if (bitNumbers.Length >= 2)
            {
                int upperBitNumber = 0;
                if (Int32.TryParse(bitNumbers[0], out upperBitNumber))
                    return upperBitNumber;
            }
            return 0;
        }

        private static int StopReason(AdamData dataValues)
        {
            if ((dataValues.DiCounters[UpperBit(Properties.Settings.Default.AdamWarpStopCounterRegister)] * (int)Math.Pow(2, 16) + dataValues.DiCounters[LowerBit(Properties.Settings.Default.AdamWarpStopCounterRegister)]) > 0)
                return Properties.Settings.Default.AdamWarpStopReasonId;
            else if ((dataValues.DiCounters[UpperBit(Properties.Settings.Default.AdamWeftStopCounterRegister)] * (int)Math.Pow(2, 16) + dataValues.DiCounters[LowerBit(Properties.Settings.Default.AdamWeftStopCounterRegister)]) > 0)
                return Properties.Settings.Default.AdamWeftStopReasonId;
            return Properties.Settings.Default.AdamStopReasonId;
        }

        private static bool IsRunning(AdamMachineData actualData, AdamMachineData previousData)
        {
            if (previousData != null)
                return (previousData.Pasadas < actualData.Pasadas);
            return actualData.Pasadas > 0;
        }

        public static AdamMachineData Create(AdamData dataValues, AdamMachineData previousData)
        {
            AdamMachineData machineData = new AdamMachineData();
            if (dataValues != null)
            {
                machineData.MachineId = dataValues.MachineId;
                machineData.ReadTime = DateTime.Now;
                machineData.Pasadas = dataValues.DiCounters[UpperBit(Properties.Settings.Default.AdamPeakCounterRegister)] * (int)Math.Pow(2, 16) + dataValues.DiCounters[LowerBit(Properties.Settings.Default.AdamPeakCounterRegister)];
                machineData.StopReason = StopReason(dataValues);
                machineData.Running = IsRunning(machineData, previousData);
                return machineData;
            }
            return null;
        }
    }
}
