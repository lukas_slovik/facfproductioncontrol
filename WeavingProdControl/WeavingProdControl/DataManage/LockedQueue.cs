﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeavingProdControl.DataManage
{
    public class LockedQueue
    {
        private Queue<AdamMachineData> runnableList = new Queue<AdamMachineData>();
        private object listLock = new object();
        public void Add(AdamMachineData task)
        {
            lock (this.listLock)
            {
                this.runnableList.Enqueue(task);
#if DEBUG
                Console.WriteLine("Add(AdamMachineData task) " + DateTime.Now);
#endif
            }
        }
        public AdamMachineData Get()
        {
            lock (this.listLock)
            {
                if (this.runnableList.Count > 0)
                    return this.runnableList.Dequeue();
                else
                    return null;
            }
        }

        public List<AdamMachineData> GetAll()
        {
            List<AdamMachineData> machineData = new List<AdamMachineData>();
            lock (this.listLock)
            {
                while (this.runnableList.Count > 0)
                {
                    machineData.Add(this.runnableList.Dequeue());
                }
            }
            return machineData;
        }
    }
}
