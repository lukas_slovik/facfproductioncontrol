﻿using AdamUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.DataAccess;
using WeavingProdControl.Log;

namespace WeavingProdControl.DataManage
{
    public class MachineAdam
    {
        private void ErrorLog(string errorMsg)
        {
            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), Machinedata.idTelar + " " + errorMsg);
        }
        public Telare Machinedata { get; private set; }
        public AdamConnector IoConnector { get; set; }
        public MachineAdam(Telare machine)
        {
            if (machine != null)
            {
                Machinedata = machine;
                IoConnector = new AdamConnector(machine.idTelar,
                    machine.DireccionIP.Trim(),
                    Properties.Settings.Default.DefaultAdamPort,
                    Properties.Settings.Default.DisconnectTimeOutAdam,
                    Properties.Settings.Default.ReadTimeOutAdam,
                    AdamDataSaver.SaveData,
                    ErrorLog,
                    true,
                    AdamDataReadout.ReadCounters);
            }
            else
                throw new NullReferenceException();
        }
    }
}
