﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeavingProdControl.DataManage
{
    public class LockedList
    {
        private List<AdamMachineData> runnableList = new List<AdamMachineData>();
        private object listLock = new object();
        public void Add(AdamMachineData machine)
        {
            lock (this.listLock)
            {
                AdamMachineData found = this.runnableList.Find(adamMachine => adamMachine.MachineId == machine.MachineId);
                if (found != null)
                    found.Pasadas = machine.Pasadas;
                else
                    this.runnableList.Add(machine);
            }
        }

        public void Remove(AdamMachineData machine)
        {
            lock (this.listLock)
            {
                if (machine != null)
                    this.runnableList.Remove(machine);
            }
        }

        public AdamMachineData GetByMachineId(int machineId)
        {
            lock (this.listLock)
            {
                return this.runnableList.Find(adamMachine => adamMachine.MachineId == machineId);
            }
        }
    }
}
