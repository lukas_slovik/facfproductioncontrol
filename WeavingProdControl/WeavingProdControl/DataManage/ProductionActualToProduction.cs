﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.DataAccess;
using WeavingProdControl.Log;

namespace WeavingProdControl.DataManage
{
    public class ProductionActualToProduction
    {
        private ProduccionActual actualData;
        private AdamMachineData data;
        public ProductionActualToProduction(ProduccionActual actualData, AdamMachineData data)
        {
            if (actualData != null)
                this.actualData = actualData;
            else
                throw new ArgumentNullException();
            if (data != null)
                this.data = data;
            else
                throw new ArgumentNullException();
        }

        private DateTime GetStartTime()
        {
            if (actualData.Inicio != null)
                return (DateTime)actualData.Inicio;
            else
            {
                EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " ProduccionActual actualData.Inicio is null ");
                throw new NullReferenceException();
            }
        }

        public Produccion Adapted()
        {
            Produccion prodData = new Produccion();
            try
            {
                prodData.Inicio = GetStartTime();
            }
            catch (Exception e)
            {
                EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                return null;
            }
            prodData.Fin = data.ReadTime;
            prodData.idTelar = this.actualData.IdTelar;
            prodData.DiaTurno = this.actualData.DiaTurno;
            prodData.Estado = this.actualData.Estado;
            prodData.Velocidad = this.actualData.Velocidad;
            prodData.Pasadas = this.actualData.Pasadas;
            prodData.CodigoParo = this.actualData.CodigoParo;
            prodData.TipoError = this.actualData.TipoError;
            if (this.actualData.Articulo != null)
                prodData.Articulo = this.actualData.Articulo.Trim().ToUpper();
            else
            {
                EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " this.actualData.Articulo is null");
                prodData.Articulo = null;
            }
            prodData.Pieza = this.actualData.Pieza;
            prodData.LongitudPieza = this.actualData.LongitudPieza;
            prodData.OrdenProduccion = this.actualData.OrdenProduccion;
            prodData.Turno = this.actualData.Turno;
            prodData.IdCodigoParo = this.actualData.idCodigoParo;
            prodData.MetrosFabricados = this.actualData.MetrosFabricados;
            prodData.MetrosMarcha = this.actualData.MetrosFabricados;
            prodData.cdOperario = this.actualData.cdOperario;
            prodData.PasadasCMTelar = this.actualData.PasadasCMTelar;
            return prodData;
        }
    }
}
