﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.DataAccess;
using WeavingProdControl.Log;

namespace WeavingProdControl.DataManage
{
    public class SpeedCounter
    {
        public static int Calculate(ProduccionActual currentData, AdamMachineData data)
        {
            if (currentData != null && data != null)
            {
                if (currentData.Inicio != null)
                {
                    TimeSpan timeDifference = (data.ReadTime - (DateTime)currentData.Inicio);
                    if (timeDifference.TotalSeconds >= Properties.Settings.Default.MinimumSecondToCalculateSpeed)
                        return (int)(data.Pasadas / timeDifference.TotalSeconds * 60);
                    else
                        EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " TimeSpan timeDifference is less that MinimumSecondToCalculateSpeed ");
                }
                else
                    throw new NullReferenceException("ProduccionActual currentData.Inicio is null");

            }
            else
                throw new ArgumentNullException();
            return 0;
        }
    }
}
