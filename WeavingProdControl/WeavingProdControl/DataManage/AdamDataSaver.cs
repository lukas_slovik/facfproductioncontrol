﻿using AdamUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.Log;

namespace WeavingProdControl.DataManage
{
    public class AdamDataSaver
    {
        public static LockedQueue DataToSave { get; private set; }
        private static LockedList previousData;
        static AdamDataSaver()
        {
            DataToSave = new LockedQueue();
            previousData = new LockedList();
        }

        public static bool SaveData(AdamData davaValues)
        {
            AdamMachineData data = AdamMachineDatoBuilder.Create(davaValues, AdamDataSaver.previousData.GetByMachineId(davaValues.MachineId));
            if (data != null)
            {
                AdamDataSaver.previousData.Add(data);
                DataToSave.Add(data);
#if DEBUG
                //return !data.Running;
                return false;
#else
                return !data.Running;
#endif
            }
            else
                EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " AdamMachineData data is null ");
            return false;
        }
    }
}
