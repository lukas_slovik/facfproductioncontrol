﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.DataAccess;
using WeavingProdControl.Log;

namespace WeavingProdControl.DataManage
{
    public class MachineStatusControl
    {
        public static bool IsRunning(ProduccionActual currentData)
        {
            if (currentData != null)
            {
                return currentData.Estado.Trim().ToUpper() == Properties.Settings.Default.MachineRunningStatus;
            }
            else
                EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod() + " ProduccionActual currentData is null ");
            return false;
        }

        public static string StopStatus()
        {
            return Properties.Settings.Default.MachineStoppedStatus;
        }

        public static string RunningStatus()
        {
            return Properties.Settings.Default.MachineRunningStatus;
        }
    }
}
