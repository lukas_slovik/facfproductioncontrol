﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.ProductionControl;

namespace WeavingProdControl.ServiceOperation
{
    public class ServiceOperations
    {
        private List<OperationCore> operations;
        public ServiceOperations()
        {
            this.operations = new List<OperationCore>();
            this.operations.Add(new DatabaseSaver());
            this.operations.Add(new IoModuleDataOperation());
        }
        public void StartServiceOperations()
        {
            foreach (OperationCore op in this.operations)
            {
                op.Start();
            }
        }

        public void StopServiceOperations()
        {
            foreach (OperationCore op in this.operations)
            {
                op.Stop();
            }
        }
    }
}
