﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WeavingProdControl.Log
{
    public class EventLogMultiThread
    {
        private static System.Diagnostics.EventLog eventLoging = new System.Diagnostics.EventLog();

        public static void AddNewLogSource(string sourceName)
        {
            if (!System.Diagnostics.EventLog.SourceExists(sourceName))
            {
                System.Diagnostics.EventLog.CreateEventSource(sourceName, Properties.Settings.Default.MainInfoLog);
            }
            eventLoging.Source = sourceName;
            eventLoging.Log = Properties.Settings.Default.MainInfoLog;
        }

        private static void AddLogEntry(string logSource, string LogEntry, System.Diagnostics.EventLogEntryType logType)
        {
            lock (eventLoging)
            {
                try
                {
                    eventLoging.Source = logSource;
                    eventLoging.WriteEntry(LogEntry, logType);
                }
                catch (Exception e)
                {
                    Console.WriteLine(System.Reflection.MethodBase.GetCurrentMethod() + " " + e.Message);
                }
            }
        }

        public static void AddErrorLog(string logSource, string LogEntry)
        {
            AddLogEntry(logSource, LogEntry, System.Diagnostics.EventLogEntryType.Error);
        }

        public static void AddErrorLog(string logSource, MethodBase classBase, string logEntry)
        {
            string fullName = "";
            if (classBase != null)
            {
                fullName = string.Format("{0}.{1}({2})", classBase.ReflectedType.FullName, classBase.Name, string.Join(",", classBase.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
            }
            AddLogEntry(logSource, fullName + " " + logEntry, System.Diagnostics.EventLogEntryType.Error);
        }

        public static void AddErrorLog(string logSource, MethodBase classBase, Exception e)
        {
            string fullName = "";
            if (classBase != null)
            {
                fullName = string.Format("{0}.{1}({2})", classBase.ReflectedType.FullName, classBase.Name, string.Join(",", classBase.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
            }
            string logEntry = "";
            if (e != null)
            {
                if (e.Message != null)
                    logEntry += e.Message + " ";
                if (e.InnerException != null)
                    if (e.InnerException.Message != null)
                        logEntry += e.InnerException.Message;
            }
            AddLogEntry(logSource, fullName + " " + logEntry, System.Diagnostics.EventLogEntryType.Error);
        }

        public static void AddInfoLog(string logSource, string LogEntry)
        {
            AddLogEntry(logSource, LogEntry, System.Diagnostics.EventLogEntryType.Information);
        }

        public static void AddWarningLog(string logSource, string LogEntry)
        {
            AddLogEntry(logSource, LogEntry, System.Diagnostics.EventLogEntryType.Warning);
        }

        public static void AddDebugLog(string logSource, string LogEntry)
        {
            if (Properties.Settings.Default.DebugLogActive)
                AddLogEntry(logSource, LogEntry, System.Diagnostics.EventLogEntryType.Warning);
        }

    }
}
