﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeavingProdControl.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeavingProdControl.DataAccess.Tests
{
    [TestClass()]
    public class PeaksPerCmDaoTests
    {
        [TestMethod()]
        public void AllDataTest()
        {
            Assert.IsTrue(PeaksPerCmDao.Instance.AllData().Count > 0);
        }

        [TestMethod()]
        public void PeaksPerItemNullTest()
        {
            Assert.IsTrue(PeaksPerCmDao.Instance.PeaksPerItem(null)==0);
        }

        [TestMethod()]
        public void PeaksPerItemEmptyTest()
        {
            Assert.IsTrue(PeaksPerCmDao.Instance.PeaksPerItem("") == 0);
        }

        [TestMethod()]
        public void PeaksPerItemCorrectTest()
        {
            Assert.IsTrue(PeaksPerCmDao.Instance.PeaksPerItem("2306301-2060T2U          ") != 0);
        }
    }
}