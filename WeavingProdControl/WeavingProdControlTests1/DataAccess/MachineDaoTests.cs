﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeavingProdControl.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeavingProdControl.DataAccess.Tests
{
    [TestClass()]
    public class MachineDaoTests
    {
        [TestMethod()]
        public void IpMachinesTest()
        {
            Assert.IsTrue(MachineDao.Instance.IpMachines().Count>0);
        }
    }
}